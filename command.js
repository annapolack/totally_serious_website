/*when home button clicked -> brings to index webpage*/
function index() {
    location.href = 'index.html';
}

/*when inside button clicked -> brings to inside webpage*/
function aboutmyself() {
    location.href = 'aboutmyself.html';
}

function latestprojects() {
    location.href = 'latestprojects.html';
}

function goals() {
    location.href = 'goals.html';
}

function outsideofwork() {
    location.href = 'outsideofwork.html';
}

